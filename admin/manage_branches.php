<?php
/**
 *
 * This file is part of HESK - PHP Help Desk Software.
 *
 * (c) Copyright Klemen Stirn. All rights reserved.
 * https://www.hesk.com
 *
 * For the full copyright and license agreement information visit
 * https://www.hesk.com/eula.php
 *
 */

define('IN_SCRIPT',1);
define('HESK_PATH','../');

/* Get all the required files and functions */
require(HESK_PATH . 'hesk_settings.inc.php');
require(HESK_PATH . 'inc/common.inc.php');
require(HESK_PATH . 'inc/admin_functions.inc.php');
hesk_load_database_functions();

hesk_session_start();
hesk_dbConnect();
hesk_isLoggedIn();

/* Check permissions for this feature */
hesk_checkPermission('can_man_branch');

// Possible priorities
$priorities = array(
	3 => array('value' => 3, 'text' => $hesklang['low'],		'formatted' => $hesklang['low']),
	2 => array('value' => 2, 'text' => $hesklang['medium'],		'formatted' => '<font class="medium">'.$hesklang['medium'].'</font>'),
	1 => array('value' => 1, 'text' => $hesklang['high'],		'formatted' => '<font class="important">'.$hesklang['high'].'</font>'),
	0 => array('value' => 0, 'text' => $hesklang['critical'],	'formatted' => '<font class="critical">'.$hesklang['critical'].'</font>'),
);

/* What should we do? */
if ( $action = hesk_REQUEST('a') )
{
	if ($action == 'linkcode')       {generate_link_code();}
	elseif ( defined('HESK_DEMO') )  {hesk_process_messages($hesklang['ddemo'], 'manage_branches.php', 'NOTICE');}
	elseif ($action == 'new')        {new_branch();}
	elseif ($action == 'rename')     {rename_branch();}
	elseif ($action == 'remove')     {remove();}
	elseif ($action == 'order')      {order_branch();}
	elseif ($action == 'autoassign') {toggle_autoassign();}
	elseif ($action == 'type')       {toggle_type();}
	elseif ($action == 'priority')   {change_priority();}
}

/* Print header */
require_once(HESK_PATH . 'inc/header.inc.php');

/* Print main manage users page */
require_once(HESK_PATH . 'inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<script language="Javascript" type="text/javascript"><!--
function confirm_delete()
{
if (confirm('<?php echo hesk_makeJsString($hesklang['confirm_del_branch']); ?>')) {return true;}
else {return false;}
}
//-->
</script>

<?php
/* This will handle error, success and notice messages */
hesk_handle_messages();
?>

<h3 style="padding-bottom:5px"><?php echo $hesklang['manage_branch']; ?> [<a href="javascript:void(0)" onclick="javascript:alert('<?php echo hesk_makeJsString($hesklang['branch_intro']); ?>')">?</a>]</h3>

&nbsp;<br />

<div align="center">
<table border="0" cellspacing="1" cellpadding="3" class="white" width="100%">
<tr>
<th class="admin_white" style="white-space:nowrap;width:1px;"><b><i>&nbsp;<?php echo $hesklang['id']; ?>&nbsp;</i></b></th>
<th class="admin_white" style="text-align:left"><b><i>&nbsp;<?php echo $hesklang['branch_name']; ?>&nbsp;</i></b></th>
<th class="admin_white" style="text-align:left"><b><i>&nbsp;<?php echo 'General Manager'; ?>&nbsp;</i></b></th>
<th class="admin_white" style="text-align:left"><b><i>&nbsp;<?php echo 'Manager'; ?>&nbsp;</i></b></th>
<th class="admin_white" style="text-align:left"><b><i>&nbsp;<?php echo $hesklang['priority']; ?>&nbsp;</i></b></th>
<th class="admin_white" style="white-space:nowrap;width:1px;"><b><i>&nbsp;<?php echo $hesklang['not']; ?>&nbsp;</i></b></th>
<th class="admin_white" style="text-align:left"><b><i>&nbsp;<?php echo $hesklang['graph']; ?>&nbsp;</i></b></th>
<th class="admin_white" style="width:100px"><b><i>&nbsp;<?php echo $hesklang['opt']; ?>&nbsp;</i></b></th>
</tr>

<?php
/* Get number of tickets per branch */
$tickets_all   = array();
$tickets_total = 0;

$res = hesk_dbQuery('SELECT COUNT(*) AS `cnt`, `branch` FROM `'.hesk_dbEscape($hesk_settings['db_pfix']).'tickets` GROUP BY `branch`');
while ($tmp = hesk_dbFetchAssoc($res))
{
	$tickets_all[$tmp['branch']] = $tmp['cnt'];
    $tickets_total += $tmp['cnt'];
}

/* Get list of branches */
$res_old = hesk_dbQuery("SELECT * FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` ORDER BY `branch_order` ASC");

$resx = hesk_dbQuery("SELECT *
FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches`
LEFT JOIN `".hesk_dbEscape($hesk_settings['db_pfix'])."users` ON `".hesk_dbEscape($hesk_settings['db_pfix'])."branches`.general_manager = `".hesk_dbEscape($hesk_settings['db_pfix'])."branches`.id");

$res = hesk_dbQuery("SELECT t1.*, t2.`name` AS `gm_name`, t3.`user` AS `manager_name`
		FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` AS `t1`
		LEFT JOIN `".hesk_dbEscape($hesk_settings['db_pfix'])."users` AS `t2` ON `t1`.`general_manager` = `t2`.`id` 
		LEFT JOIN `".hesk_dbEscape($hesk_settings['db_pfix'])."users` AS `t3` ON `t1`.`manager` = `t3`.`id` 
		");

// echo '<pre>';
// print_r($res);
// // print_r($resx);
// echo '<pre/>';

$str = 'one,two,three,four';

// zero limit
// print_r(explode(',',$str,0));


$options ='';

$i = 1;
$j = 0;
$num = hesk_dbNumRows($res);

while ($mybranch = hesk_dbFetchAssoc($res))
{
	$j++;

    if (isset($_SESSION['selbranch2']) && $mybranch['id'] == $_SESSION['selbranch2'])
    {
		$color = 'admin_green';
		unset($_SESSION['selbranch2']);
	}
    else
    {
		$color = $i ? 'admin_white' : 'admin_gray';
    }

	$tmp   = $i ? 'White' : 'Blue';
    $style = 'class="option'.$tmp.'OFF" onmouseover="this.className=\'option'.$tmp.'ON\'" onmouseout="this.className=\'option'.$tmp.'OFF\'"';
    $i     = $i ? 0 : 1;

    /* Number of tickets and graph width */
	$all = isset($tickets_all[$mybranch['id']]) ? $tickets_all[$mybranch['id']] : 0;
	$width_all = 0;
	if ($tickets_total && $all)
	{
		$width_all  = round(($all / $tickets_total) * 100);
	}

    /* Deleting branch with ID 1 (default branch) is not allowed */
    if ($mybranch['id'] == 1)
    {
        $remove_code=' <img src="../img/blank.gif" width="16" height="16" alt="" style="padding:3px;border:none;" />';
    }
    else
    {
        $remove_code=' <a href="manage_branches.php?a=remove&amp;branchid='.$mybranch['id'].'&amp;token='.hesk_token_echo(0).'" onclick="return confirm_delete();"><img src="../img/delete.png" width="16" height="16" alt="'.$hesklang['remove'].'" title="'.$hesklang['remove'].'" '.$style.' /></a>';
    }

	/* Is branch private or public? */
	if ($mybranch['type'])
	{
		$type_code = '<a href="manage_branches.php?a=type&amp;s=0&amp;branchid='.$mybranch['id'].'&amp;token='.hesk_token_echo(0).'"><img src="../img/private.png" width="16" height="16" alt="'.$hesklang['branch_private'].'" title="'.$hesklang['branch_private'].'" '.$style.' /></a>';
	}
	else
	{
		$type_code = '<a href="manage_branches.php?a=type&amp;s=1&amp;branchid='.$mybranch['id'].'&amp;token='.hesk_token_echo(0).'"><img src="../img/public.png" width="16" height="16" alt="'.$hesklang['branch_public'].'" title="'.$hesklang['branch_public'].'" '.$style.' /></a>';
	}

	/* Is auto assign enabled? */
	if ($hesk_settings['autoassign'])
    {
    	if ($mybranch['autoassign'])
        {
			$autoassign_code = '<a href="manage_branches.php?a=autoassign&amp;s=0&amp;branchid='.$mybranch['id'].'&amp;token='.hesk_token_echo(0).'"><img src="../img/autoassign_on.png" width="16" height="16" alt="'.$hesklang['aaon'].'" title="'.$hesklang['aaon'].'" '.$style.' /></a>';
        }
        else
        {
			$autoassign_code = '<a href="manage_branches.php?a=autoassign&amp;s=1&amp;branchid='.$mybranch['id'].'&amp;token='.hesk_token_echo(0).'"><img src="../img/autoassign_off.png" width="16" height="16" alt="'.$hesklang['aaoff'].'" title="'.$hesklang['aaoff'].'" '.$style.' /></a>';
        }
    }
    else
    {
		$autoassign_code = '';
    }

    $options .= '<option value="'.$mybranch['id'].'" ';
    $options .= (isset($_SESSION['selbranch']) && $mybranch['id'] == $_SESSION['selbranch']) ? ' selected="selected" ' : '';
    $options .= '>'.$mybranch['name'].'</option>';


$branches = explode(',', $mybranch['general_manager'],0);

// echo '<pre>';
// // print_r($res);
// // print_r($branches);
// $comma_separated = implode(",", $branches);
// print_r($comma_separated);
// echo '<pre/>';


	echo '
	<tr>
	<td class="'.$color.'">'.$mybranch['id'].'</td>
	<td class="'.$color.'">'.$mybranch['name'].'</td>
	<td class="'.$color.'">'.$mybranch['gm_name'].'</td>
	<td class="'.$color.'">'.$mybranch['manager_name'].'</td>
	<td class="'.$color.'" width="1" style="white-space: nowrap;">'.$priorities[$mybranch['priority']]['formatted'].'&nbsp;</td>
	<td class="'.$color.'" style="text-align:center"><a href="show_tickets.php?branch='.$mybranch['id'].'&amp;s_all=1&amp;s_my=1&amp;s_ot=1&amp;s_un=1" alt="'.$hesklang['list_tickets_branch'].'" title="'.$hesklang['list_tickets_branch'].'">'.$all.'</a></td>
	<td class="'.$color.'" width="1">
	<div class="progress-container" style="width: 160px" title="'.sprintf($hesklang['perat'],$width_all.'%').'">
	<div style="width: '.$width_all.'%;float:left;"></div>
	</div>
	</td>
	<td class="'.$color.'" style="text-align:center; white-space:nowrap;">
	<a href="Javascript:void(0)" onclick="Javascript:hesk_window(\'manage_branches.php?a=linkcode&amp;branchid='.$mybranch['id'].'&amp;p='.$mybranch['type'].'\',\'200\',\'500\')"><img src="../img/code' . ($mybranch['type'] ? '_off' : '') . '.png" width="16" height="16" alt="'.$hesklang['geco'].'" title="'.$hesklang['geco'].'" '.$style.' /></a>
	' . $autoassign_code . '
    ' . $type_code . ' ';

	if ($num > 1)
	{
		if ($j == 1)
		{
			echo'<img src="../img/blank.gif" width="16" height="16" alt="" style="padding:3px;border:none;" /> <a href="manage_branches.php?a=order&amp;branchid='.$mybranch['id'].'&amp;move=15&amp;token='.hesk_token_echo(0).'"><img src="../img/move_down.png" width="16" height="16" alt="'.$hesklang['move_dn'].'" title="'.$hesklang['move_dn'].'" '.$style.' /></a>';
		}
		elseif ($j == $num)
		{
			echo'<a href="manage_branches.php?a=order&amp;branchid='.$mybranch['id'].'&amp;move=-15&amp;token='.hesk_token_echo(0).'"><img src="../img/move_up.png" width="16" height="16" alt="'.$hesklang['move_up'].'" title="'.$hesklang['move_up'].'" '.$style.' /></a> <img src="../img/blank.gif" width="16" height="16" alt="" style="padding:3px;border:none;" />';
		}
		else
		{
			echo'
			<a href="manage_branches.php?a=order&amp;branchid='.$mybranch['id'].'&amp;move=-15&amp;token='.hesk_token_echo(0).'"><img src="../img/move_up.png" width="16" height="16" alt="'.$hesklang['move_up'].'" title="'.$hesklang['move_up'].'" '.$style.' /></a>
			<a href="manage_branches.php?a=order&amp;branchid='.$mybranch['id'].'&amp;move=15&amp;token='.hesk_token_echo(0).'"><img src="../img/move_down.png" width="16" height="16" alt="'.$hesklang['move_dn'].'" title="'.$hesklang['move_dn'].'" '.$style.' /></a>
			';
		}
	}

    echo $remove_code.'</td>
	</tr>
	';

} // End while

?>
</table>
</div>

<p>&nbsp;</p>

<?php
if ($hesk_settings['cust_urgency'])
{
	hesk_show_notice($hesklang['branch_pri_info'] . ' ' . $hesklang['cpri']);
}
?>

<!-- add branch section -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" height="7"><img src="../img/roundcornerslt.jpg" width="7" height="7" alt="" /></td>
		<td class="roundcornerstop"></td>
		<td><img src="../img/roundcornersrt.jpg" width="7" height="7" alt="" /></td>
	</tr>
	<tr>
		<td class="roundcornersleft">&nbsp;</td>
		<td>
        <!-- CONTENT -->
		<form action="manage_branches.php" method="post">
			<h3>&raquo; <?php echo $hesklang['add_branch']; ?></h3>
			<p>
				<b><?php echo $hesklang['branch_name']; ?></b> (<?php echo $hesklang['max_chars']; ?>)<b>:</b><br />
				<input type="text" name="name" size="40" maxlength="40"
					<?php
						if (isset($_SESSION['branchname']))
					    {
					    	echo ' value="'.hesk_input($_SESSION['branchname']).'" ';
					    }
					?>
				/>
			</p>
			<p>
				<b><?php echo $hesklang['def_branch_pri']; ?></b> 
				[<b><a href="javascript:void(0)" onclick="javascript:alert('<?php echo hesk_makeJsString($hesklang['branch_pri']); ?>')">?</a></b>]
				<br />
				<select name="priority">
				<?php
				// Default priority: low
				if ( ! isset($_SESSION['branch_priority']) )
				{
					$_SESSION['branch_priority'] = 3;
				}

				// List possible priorities
				foreach ($priorities as $value => $info)
				{
					echo '<option value="'.$value.'"'.($_SESSION['branch_priority'] == $value ? ' selected="selected"' : '').'>'.$info['text'].'</option>';
				}
				?>
				</select>
			</p>
			<?php/*<!-- Branch GM -->
			<p>
				<b><?php echo 'Branch GM'; ?></b> 
				[<b><a href="javascript:void(0)" onclick="javascript:alert('<?php echo hesk_makeJsString($hesklang['branch_pri']); ?>')">?</a></b>]
				<br />
				<select name="priority">
				<?php
				// Default priority: low
				if ( ! isset($_SESSION['branch_priority']) )
				{
					$_SESSION['branch_priority'] = 3;
				}

				// List possible priorities
				foreach ($priorities as $value => $info)
				{
					echo '<option value="'.$value.'"'.($_SESSION['branch_priority'] == $value ? ' selected="selected"' : '').'>'.$info['text'].'</option>';
				}
				?>
				</select>
			</p>
			<!-- Branch GM -->
			<!-- Branch Manager -->
			<p>
				<b><?php echo 'Branch Manager'; ?></b> 
				[<b><a href="javascript:void(0)" onclick="javascript:alert('<?php echo hesk_makeJsString($hesklang['branch_pri']); ?>')">?</a></b>]
				<br />
				<select name="priority">
				<?php
				// Default priority: low
				if ( ! isset($_SESSION['branch_priority']) )
				{
					$_SESSION['branch_priority'] = 3;
				}

				// List possible priorities
				foreach ($priorities as $value => $info)
				{
					echo '<option value="'.$value.'"'.($_SESSION['branch_priority'] == $value ? ' selected="selected"' : '').'>'.$info['text'].'</option>';
				}
				?>
				</select>
			</p>
			<!-- Branch Manager -->*/?>
			<p>
				<b><?php echo $hesklang['opt']; ?>:</b><br />

				<?php
				if ($hesk_settings['autoassign'])
				{
					?>
					<label>
						<input type="checkbox" name="autoassign" value="Y" <?php if ( ! isset($_SESSION['branch_autoassign']) || $_SESSION['branch_autoassign'] == 1 ) {echo 'checked="checked"';} ?>  /> 
						<?php echo $hesklang['branch_aa']; ?>
					</label>
					<br />
					<?php
				}
				?>
				<label>
					<input type="checkbox" name="type" value="Y" <?php if ( isset($_SESSION['branch_type']) && $_SESSION['branch_type'] == 1 ) {echo 'checked="checked"';} ?> /> 
					<?php echo $hesklang['branch_type']; ?>
				</label>
				<br />
			</p>

			<input type="hidden" name="a" value="new" />
			<input type="hidden" name="token" value="<?php hesk_token_echo(); ?>" />
			<input type="submit" value="<?php echo $hesklang['create_branch']; ?>" class="orangebutton" onmouseover="hesk_btn(this,'orangebuttonover');" onmouseout="hesk_btn(this,'orangebutton');" />
		</form>
		<!-- END CONTENT -->
        </td>
		<td class="roundcornersright">&nbsp;</td>
	</tr>
	<tr>
		<td><img src="../img/roundcornerslb.jpg" width="7" height="7" alt="" /></td>
		<td class="roundcornersbottom"></td>
		<td width="7" height="7"><img src="../img/roundcornersrb.jpg" width="7" height="7" alt="" /></td>
	</tr>
</table>
<!-- end add branch section -->

<p>&nbsp;</p>

<!-- rename branches section -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" height="7"><img src="../img/roundcornerslt.jpg" width="7" height="7" alt="" /></td>
		<td class="roundcornerstop"></td>
		<td><img src="../img/roundcornersrt.jpg" width="7" height="7" alt="" /></td>
	</tr>
	<tr>
		<td class="roundcornersleft">&nbsp;</td>
		<td>
        <!-- CONTENT -->
		<form action="manage_branches.php" method="post">
			<h3>&raquo; <?php echo $hesklang['ren_branch']; ?></h3>
			<table border="0" style="margin-top:10px;">
				<tr>
					<td><?php echo $hesklang['oln']; ?></td>
					<td><select name="branchid"><?php echo $options; ?></select></td>
				</tr>
				<tr>
					<td><?php echo $hesklang['nen']; ?></td>
					<td><input type="text" name="name" size="40" maxlength="40" <?php if (isset($_SESSION['branchname2'])) {echo ' value="'.hesk_input($_SESSION['branchname2']).'" ';} ?> /></td>
				</tr>
			</table>
			<p>
				<input type="hidden" name="a" value="rename" />
				<input type="hidden" name="token" value="<?php hesk_token_echo(); ?>" />
				<input type="submit" value="<?php echo $hesklang['ren_branch']; ?>" class="orangebutton" onmouseover="hesk_btn(this,'orangebuttonover');" onmouseout="hesk_btn(this,'orangebutton');" />
			</p>
		</form>
		<!-- END CONTENT -->
        </td>
		<td class="roundcornersright">&nbsp;</td>
	</tr>
	<tr>
		<td><img src="../img/roundcornerslb.jpg" width="7" height="7" alt="" /></td>
		<td class="roundcornersbottom"></td>
		<td width="7" height="7"><img src="../img/roundcornersrb.jpg" width="7" height="7" alt="" /></td>
	</tr>
</table>
<!-- end rename branches section -->
<p>&nbsp;</p>

<!--  branch priority section -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" height="7"><img src="../img/roundcornerslt.jpg" width="7" height="7" alt="" /></td>
		<td class="roundcornerstop"></td>
		<td><img src="../img/roundcornersrt.jpg" width="7" height="7" alt="" /></td>
	</tr>
	<tr>
		<td class="roundcornersleft">&nbsp;</td>
		<td>
        <!-- CONTENT -->
		<form action="manage_branches.php" method="post">
			<h3>&raquo; <?php echo $hesklang['ch_branch_pri']; ?></h3>
			<table border="0" style="margin-top:10px;">
				<tr>
					<td><?php echo $hesklang['branch']; ?>:</td>
					<td><select name="branchid"><?php echo $options; ?></select></td>
				</tr>
				<tr>
					<td><?php echo $hesklang['priority']; ?>:</td>
					<td><select name="priority">
						<?php
						// Default priority: low
						if ( ! isset($_SESSION['branch_ch_priority']) )
						{
							$_SESSION['branch_ch_priority'] = 3;
						}

						// List possible priorities
						foreach ($priorities as $value => $info)
						{
							echo '<option value="'.$value.'"'.($_SESSION['branch_ch_priority'] == $value ? ' selected="selected"' : '').'>'.$info['text'].'</option>';
						}
						?>
						</select>
					</td>
				</tr>
			</table>

			<p>
				<input type="hidden" name="a" value="priority" />
				<input type="hidden" name="token" value="<?php hesk_token_echo(); ?>" />
				<input type="submit" value="<?php echo $hesklang['ch_branch_pri']; ?>" class="orangebutton" onmouseover="hesk_btn(this,'orangebuttonover');" onmouseout="hesk_btn(this,'orangebutton');" />
			</p>
		</form>
		<!-- END CONTENT -->
        </td>
		<td class="roundcornersright">&nbsp;</td>
	</tr>
	<tr>
		<td><img src="../img/roundcornerslb.jpg" width="7" height="7" alt="" /></td>
		<td class="roundcornersbottom"></td>
		<td width="7" height="7"><img src="../img/roundcornersrb.jpg" width="7" height="7" alt="" /></td>
	</tr>
</table>
<!--  branch priority section -->


<!-- HR -->
<p>&nbsp;</p>

<?php
require_once(HESK_PATH . 'inc/footer.inc.php');
exit();


/*** START FUNCTIONS ***/

function change_priority()
{
	global $hesk_settings, $hesklang, $priorities;

	/* A security check */
	hesk_token_check('POST');

	$_SERVER['PHP_SELF'] = 'manage_branches.php?branchid='.intval( hesk_POST('branchid') );

	$branchid = hesk_isNumber( hesk_POST('branchid'), $hesklang['choose_branch_ren'], $_SERVER['PHP_SELF']);
	$_SESSION['selbranch'] = $branchid;
	$_SESSION['selbranch2'] = $branchid;

	$priority = intval( hesk_POST('priority', 3));
	if ( ! array_key_exists($priority, $priorities) )
	{
		$priority = 3;
	}

	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `priority`='{$priority}' WHERE `id`='".intval($branchid)."'");

    hesk_cleanSessionVars('branch_ch_priority');

	hesk_process_messages($hesklang['branch_pri_ch'].' '.$priorities[$priority]['formatted'],$_SERVER['PHP_SELF'],'SUCCESS');
} // END change_priority()


function generate_link_code() {
	global $hesk_settings, $hesklang;
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML; 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
	<title><?php echo $hesklang['genl']; ?></title>
	<meta http-equiv="Content-Type" content="text/html;charset=<?php echo $hesklang['ENCODING']; ?>" />
	<style type="text/css">
		body{ margin:5px 5px; padding:0; background:#fff; color: black; font : 68.8%/1.5 Verdana, Geneva, Arial, Helvetica, sans-serif;}
		p{ color : black; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 1.0em;}
		h3 { color : #AF0000; font-family : Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight: bold; font-size: 1.0em;}
	</style>
	</head>
	<body>

	<div style="text-align:center">
		<h3><?php echo $hesklang['genl']; ?></h3>

		<?php
		if ( ! empty($_GET['p']) )
		{
			echo '<p>&nbsp;<br />' . $hesklang['cpric'] . '<br />&nbsp;</p>';
		}
		else
		{
			?>
			<p><i><?php echo $hesklang['genl2']; ?></i></p>

			<textarea rows="3" cols="50" onfocus="this.select()"><?php echo $hesk_settings['hesk_url'].'/index.php?a=add&amp;branchid='.intval( hesk_GET('branchid') ); ?></textarea>
			<?php
		}
		?>

		<p align="center"><a href="#" onclick="Javascript:window.close()"><?php echo $hesklang['cwin']; ?></a></p>
	</div>
	</body>
	</html>
	<?php
	    exit();
}


function new_branch()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check('POST');

    /* Options */
    $_SESSION['branch_autoassign'] = hesk_POST('autoassign') == 'Y' ? 1 : 0;
    $_SESSION['branch_type'] = hesk_POST('type') == 'Y' ? 1 : 0;

	// Default priority
	$_SESSION['branch_priority'] = intval( hesk_POST('priority', 3) );
	if ($_SESSION['branch_priority'] < 0 || $_SESSION['branch_priority'] > 3)
	{
		$_SESSION['branch_priority'] = 3;
	}

    /* Category name */
	$branchname = hesk_input( hesk_POST('name') , $hesklang['enter_branch_name'], 'manage_branches.php');

    /* Do we already have a branch with this name? */
	$res = hesk_dbQuery("SELECT `id` FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` WHERE `name` LIKE '".hesk_dbEscape( hesk_dbLike($branchname) )."' LIMIT 1");
    if (hesk_dbNumRows($res) != 0)
    {
		$_SESSION['branchname'] = $branchname;
		hesk_process_messages($hesklang['cndupl'],'manage_branches.php');
    }

	/* Get the latest branch_order */
	$res = hesk_dbQuery("SELECT `branch_order` FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` ORDER BY `branch_order` DESC LIMIT 1");
	$row = hesk_dbFetchRow($res);
	$my_order = $row[0]+10;

	hesk_dbQuery("INSERT INTO `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` (`name`,`branch_order`,`autoassign`,`type`, `priority`) VALUES ('".hesk_dbEscape($branchname)."','".intval($my_order)."','".intval($_SESSION['branch_autoassign'])."','".intval($_SESSION['branch_type'])."','{$_SESSION['branch_priority']}')");

    hesk_cleanSessionVars('branchname');
    hesk_cleanSessionVars('branch_autoassign');
    hesk_cleanSessionVars('branch_type');
    hesk_cleanSessionVars('branch_priority');

    $_SESSION['selbranch2'] = hesk_dbInsertID();

	hesk_process_messages(sprintf($hesklang['branch_name_added'],'<i>'.stripslashes($branchname).'</i>'),'manage_branches.php','SUCCESS');
} // End new_branch()


function rename_branch()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check('POST');

    $_SERVER['PHP_SELF'] = 'manage_branches.php?branchid='.intval( hesk_POST('branchid') );

	$branchid = hesk_isNumber( hesk_POST('branchid'), $hesklang['choose_branch_ren'], $_SERVER['PHP_SELF']);
	$_SESSION['selbranch'] = $branchid;
    $_SESSION['selbranch2'] = $branchid;

	$branchname = hesk_input( hesk_POST('name'), $hesklang['branch_ren_name'], $_SERVER['PHP_SELF']);
    $_SESSION['branchname2'] = $branchname;

	$res = hesk_dbQuery("SELECT `id` FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` WHERE `name` LIKE '".hesk_dbEscape( hesk_dbLike($branchname) )."' LIMIT 1");
    if (hesk_dbNumRows($res) != 0)
    {
    	$old = hesk_dbFetchAssoc($res);
        if ($old['id'] == $branchid)
        {
        	hesk_process_messages($hesklang['noch'],$_SERVER['PHP_SELF'],'NOTICE');
        }
        else
        {
    		hesk_process_messages($hesklang['cndupl'],$_SERVER['PHP_SELF']);
        }
    }

	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `name`='".hesk_dbEscape($branchname)."' WHERE `id`='".intval($branchid)."'");

    unset($_SESSION['selbranch']);
    unset($_SESSION['branchname2']);

    hesk_process_messages($hesklang['branch_renamed_to'].' <i>'.stripslashes($branchname).'</i>',$_SERVER['PHP_SELF'],'SUCCESS');
} // End rename_branch()


function remove()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check();

    $_SERVER['PHP_SELF'] = 'manage_branches.php';

	$mybranch = intval( hesk_GET('branchid') ) or hesk_error($hesklang['no_branch_id']);
	if ($mybranch == 1)
    {
    	hesk_process_messages($hesklang['cant_del_default_branch'],$_SERVER['PHP_SELF']);
    }

	hesk_dbQuery("DELETE FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` WHERE `id`='".intval($mybranch)."'");
	if (hesk_dbAffectedRows() != 1)
    {
    	hesk_error("$hesklang[int_error]: $hesklang[branch_not_found].");
    }

	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."tickets` SET `branch`=1 WHERE `branch`='".intval($mybranch)."'");

    hesk_process_messages($hesklang['branch_removed_db'],$_SERVER['PHP_SELF'],'SUCCESS');
} // End remove()


function order_branch()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check();

	$branchid = intval( hesk_GET('branchid') ) or hesk_error($hesklang['branch_move_id']);
	$_SESSION['selbranch2'] = $branchid;

	$branch_move=intval( hesk_GET('move') );

	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `branch_order`=`branch_order`+".intval($branch_move)." WHERE `id`='".intval($branchid)."'");
	if (hesk_dbAffectedRows() != 1)
    {
    	hesk_error("$hesklang[int_error]: $hesklang[branch_not_found].");
    }

	/* Update all branch fields with new order */
	$res = hesk_dbQuery("SELECT `id` FROM `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` ORDER BY `branch_order` ASC");

	$i = 10;
	while ($mybranch=hesk_dbFetchAssoc($res))
	{
	    hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `branch_order`=".intval($i)." WHERE `id`='".intval($mybranch['id'])."'");
	    $i += 10;
	}

    header('Location: manage_branches.php');
    exit();
} // End order_branch()


function toggle_autoassign()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check();

	$branchid = intval( hesk_GET('branchid') ) or hesk_error($hesklang['branch_move_id']);
	$_SESSION['selbranch2'] = $branchid;

    if ( intval( hesk_GET('s') ) )
    {
		$autoassign = 1;
        $tmp = $hesklang['caaon'];
    }
    else
    {
        $autoassign = 0;
        $tmp = $hesklang['caaoff'];
    }

	/* Update auto-assign settings */
	$res = hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `autoassign`='".intval($autoassign)."' WHERE `id`='".intval($branchid)."'");
	if (hesk_dbAffectedRows() != 1)
    {
        hesk_process_messages($hesklang['int_error'].': '.$hesklang['branch_not_found'],'./manage_branches.php');
    }

    hesk_process_messages($tmp,'./manage_branches.php','SUCCESS');

} // End toggle_autoassign()


function toggle_type()
{
	global $hesk_settings, $hesklang;

	/* A security check */
	hesk_token_check();

	$branchid = intval( hesk_GET('branchid') ) or hesk_error($hesklang['branch_move_id']);
	$_SESSION['selbranch2'] = $branchid;

    if ( intval( hesk_GET('s') ) )
    {
		$type = 1;
        $tmp = $hesklang['cpriv'];
    }
    else
    {
        $type = 0;
        $tmp = $hesklang['cpub'];
    }

	/* Update auto-assign settings */
	hesk_dbQuery("UPDATE `".hesk_dbEscape($hesk_settings['db_pfix'])."branches` SET `type`='{$type}' WHERE `id`='".intval($branchid)."'");
	if (hesk_dbAffectedRows() != 1)
    {
        hesk_process_messages($hesklang['int_error'].': '.$hesklang['branch_not_found'],'./manage_branches.php');
    }

    hesk_process_messages($tmp,'./manage_branches.php','SUCCESS');

} // End toggle_type()
?>
