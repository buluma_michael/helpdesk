<?php
// ini_set('display_errors','off');
define('IN_SCRIPT',1);
define('HESK_PATH','/home/toyota/public_html/helpdesk/');

// Try to detect some simple SPAM bots
/*if ( ! isset($_POST['hx']) || $_POST['hx'] != 3 || ! isset($_POST['hy']) || $_POST['hy'] != '' || isset($_POST['phone']) )
{
	header('HTTP/1.1 403 Forbidden');
	exit();
}
*/

// Get all the required files and functions
require(HESK_PATH . 'hesk_settings.inc.php');
require(HESK_PATH . 'inc/common.inc.php');

hesk_load_database_functions();
require(HESK_PATH . 'inc/email_functions.inc.php');
require(HESK_PATH . 'inc/posting_functions.inc.php');


function __post_ticket_data($formdata){
  global $hesk_settings, $hesklang;
	// get the category id and branch id
  //
	$res = hesk_dbQuery("SELECT * FROM `hesk_categories` WHERE `name` LIKE '%".$formdata['category']."%'");
  $res2 = hesk_dbQuery("SELECT * FROM `hesk_branches` WHERE `name` LIKE '%".$formdata['branch']."%'");
	//$category_id;
  $category = hesk_dbFetchAssoc($res);
  $branch = hesk_dbFetchAssoc($res2);
  $formdata['category'] = $category['id'];
  $formdata['branch'] = $branch['id'];
  $formdata['email2'] = $formdata['email'];
  $email_available = true;
//   print_r($formdata);

  //return do_post_request('submit_ticket.php', $formdata);

  $tmpvar['category'] = $formdata['category'];
  $tmpvar['branch'] = $formdata['branch'];

  // set priority as that of category
  $tmpvar['priority'] = $category['priority'];
  $tmpvar['subject'] = $formdata['subject'];
  $tmpvar['message'] = $formdata['message'];

  $tmpvar['trackid'] = hesk_createID();

  $tmpvar['message']=hesk_makeURL($tmpvar['message']);
  $tmpvar['message']=nl2br($tmpvar['message']);

  // All good now, continue with ticket creation
  $tmpvar['owner']   = 0;
  $tmpvar['history'] = sprintf($hesklang['thist15'], hesk_date(), $tmpvar['name']);

  // Auto assign tickets if aplicable
  // to a category & branch
  $autoassign_owner = hesk_autoAssignTicket($tmpvar['branch'], $tmpvar['category']);
  if ($autoassign_owner)
  {
    $tmpvar['owner']    = $autoassign_owner['id'];
    $tmpvar['history'] .= sprintf($hesklang['thist10'], hesk_date(), $autoassign_owner['name'].' ('.$autoassign_owner['user'].')');
  }

  // Insert ticket to database
  global $ticket;
  $ticket = hesk_newTicket(array_merge($formdata, $tmpvar));

  // Notify the customer
  if ($hesk_settings['notify_new'] && $email_available)
  {
    hesk_notifyCustomer();
  }
  // notify staff?
  // --> From autoassign?
  if ($tmpvar['owner'] && $autoassign_owner['notify_assigned'])
  {
    hesk_notifyAssignedStaff($autoassign_owner, 'ticket_assigned_to_you');
  }
  // --> No autoassign, find and notify appropriate staff
  elseif ( ! $tmpvar['owner'] )
  {
    hesk_notifyStaff('new_ticket_staff', " `notify_new_unassigned` = '1' ");
  }

  // Show success message with link to ticket
  /*
  hesk_show_success(
    $hesklang['ticket_submitted'] . '<br /><br />' .
    $hesklang['ticket_submitted_success'] . ': <b>' . $ticket['trackid'] . '</b><br /><br /> ' .
    ( ! $email_available ? $hesklang['write_down'] . '<br /><br />' : '') .
    ($email_available && $hesk_settings['notify_new'] && $hesk_settings['spam_notice'] ? $hesklang['spam_inbox'] . '<br /><br />' : '') .
    '<a href="' . $hesk_settings['hesk_url'] . '/ticket.php?track=' . $ticket['trackid'] . '">' . $hesklang['view_your_ticket'] . '</a>'

  );
  */

  // Any other messages to display?
  hesk_handle_messages();


}


function post_ticket_data($formdata){
  global $hesk_settings, $hesklang;
	
  $brand = $formdata['brand']; // Toyota, Hino, Yamaha, CASE
  // echo '<pre>';
  // print_r($formdata);
  // echo '</pre>';

  switch ($brand) {
    case 'Toyota':
      // Nairobi Toyota, Westlands Toyota, Eldoret
    //   $teams = ('Nairobi Toyota'); 
      $teams = array('Nairobi Toyota','Westlands Toyota','Kirinyaga Road Toyota','Ngong Road Toyota','Mombasa Toyota','Eldoret Toyota','Lodwar Toyota','Nakuru Toyota','Nyeri Toyota','Nanyuki Toyota','Kericho Toyota','Kisumu Toyota'); 
      break;
    case 'Yamaha':
      $teams = array('Nairobi Yamaha','Kisumu Yamaha','Mombasa Yamaha'); 
      break;
    case 'CASE':
      $teams = array('Nairobi CASE','Nakuru CASE','Eldoret CASE'); 
    break;
    case 'HINO':
    case 'Hino':
      $teams = array('Kisumu HINO','Ngong Road HINO');
    break;
    
    default:
      # code...
      break;
  }
  if (in_array($formdata['branch']. ' '.$brand, $teams)){
    // 
      //echo 'selected branch is available';
     $formdata['branch_name'] = $formdata['branch']. ' '.$brand;
  }
  else {
    // not available, try to assign to another team/branch
    //echo 'selected branch is not available for the brand';
    $random = array_rand($teams);
    //$random = 1;
    //echo ' random branch is ' .$random;
    $random_branch_name = $teams[$random];
    $formdata['branch_name'] = $random_branch_name;
  }

  // echo '<pre>';
  // print_r($teams);
  // echo '</pre>';

  
  // echo '<pre>';
  // print_r($formdata);
  // echo '</pre>';
  // get the category id and branch id
  //
	$res = hesk_dbQuery("SELECT * FROM `hesk_categories` WHERE `name` LIKE '%".$formdata['category']."%'");
  $res2 = hesk_dbQuery("SELECT * FROM `hesk_branches` WHERE `name` LIKE '%".$formdata['branch_name']."%'");
	//$category_id;
  $category = hesk_dbFetchAssoc($res);
  $branch = hesk_dbFetchAssoc($res2);

  $formdata['category'] = $category['id'];
  $formdata['branch'] = $branch['id'];
  $formdata['email2'] = $formdata['email'];
  
  $email_available = true;
  //print_r($formdata);

  // echo '<pre>';
  // print_r($branch);
  // echo '</pre>';

  //exit;

  // build out branch team name based on Brand and Branch selected

  $tmpvar['category'] = $formdata['category'];
  $tmpvar['branch'] = $formdata['branch'];

  // set priority as that of category
  $tmpvar['priority'] = $category['priority'];
  $tmpvar['subject'] = $formdata['subject'];
  $tmpvar['message'] = $formdata['message'];

  $tmpvar['trackid'] = hesk_createID();

  $tmpvar['message']=hesk_makeURL($tmpvar['message']);
  $tmpvar['message']=nl2br($tmpvar['message']);

  // All good now, continue with ticket creation
  $tmpvar['owner']   = 0;
  $tmpvar['history'] = sprintf($hesklang['thist15'], hesk_date(), $tmpvar['name']);

  // Auto assign tickets if aplicable
  // to a category & branch
  $autoassign_owner = hesk_autoAssignTicket($tmpvar['branch'], $tmpvar['category'], $teams, $brand, $formdata['branch_name']);
  if ($autoassign_owner)
  {
    $tmpvar['owner']    = $autoassign_owner['id'];
    $tmpvar['history'] .= sprintf($hesklang['thist10'], hesk_date(), $autoassign_owner['name'].' ('.$autoassign_owner['user'].')');
  }

  // echo '<pre>';
  // echo 'autoassign_owner ';
  // print_r($autoassign_owner);
  // echo '</pre>';

  // echo '<pre>';
  // echo 'tmpvar ';
  // print_r($tmpvar);
  // echo '</pre>';

  //exit;

  // Insert ticket to database
  global $ticket;
  $ticket = hesk_newTicket(array_merge($formdata, $tmpvar));

  // Notify the customer
  if ($hesk_settings['notify_new'] && $email_available)
  {
    hesk_notifyCustomer();
  }
  // notify staff?
  // --> From autoassign?
  if ($tmpvar['owner'] && $autoassign_owner['notify_assigned'])
  {
    hesk_notifyAssignedStaff($autoassign_owner, 'ticket_assigned_to_you');
  }
  // --> No autoassign, find and notify appropriate staff
  elseif ( ! $tmpvar['owner'] )
  {
    hesk_notifyStaff('new_ticket_staff', " `notify_new_unassigned` = '1' ");
  }

  // Show success message with link to ticket
  /*
  hesk_show_success(
    $hesklang['ticket_submitted'] . '<br /><br />' .
    $hesklang['ticket_submitted_success'] . ': <b>' . $ticket['trackid'] . '</b><br /><br /> ' .
    ( ! $email_available ? $hesklang['write_down'] . '<br /><br />' : '') .
    ($email_available && $hesk_settings['notify_new'] && $hesk_settings['spam_notice'] ? $hesklang['spam_inbox'] . '<br /><br />' : '') .
    '<a href="' . $hesk_settings['hesk_url'] . '/ticket.php?track=' . $ticket['trackid'] . '">' . $hesklang['view_your_ticket'] . '</a>'

  );
  */

  // Any other messages to display?
  hesk_handle_messages();


}