<?php
define('IN_SCRIPT',1);
define('HESK_PATH','././');

// Get all the required files and functions
require(HESK_PATH . 'hesk_settings.inc.php');
require(HESK_PATH . 'inc/common.inc.php');

hesk_load_database_functions();
require(HESK_PATH . 'inc/email_functions.inc.php');
require(HESK_PATH . 'inc/posting_functions.inc.php');

// Load statuses
require_once(HESK_PATH . 'inc/statuses.inc.php');

// load header
require(HESK_PATH . 'inc/header.inc.php');

function get_tickets($period = '', $type = ''){
    global $hesk_settings, $hesklang, $ticket;
    // get tickets
    $columns = 't.`id`, t.`trackid`, t.`name`, t.`email`, t.`subject`, t.`message`, t.`dt` as created_on, t.`status`, 
    t.`staffreplies`, t.`owner`, t.`closedat`, t.`branch`, t.`category`, b.`name` as branchname, c.`name` as categoryname,
    u.`id` as userid, u.`user` as username, u.`name` as staffname, u.`email` as staffemail';

    $query = "SELECT ".$columns." FROM `hesk_tickets` t ";
    $query .= "LEFT JOIN `hesk_users` u on t.`owner` = u.`id` ";
    $query .= "LEFT JOIN `hesk_branches` b on t.`branch` = b.`id` ";
    $query .= "LEFT JOIN `hesk_categories` c on t.`category` = c.`id` ";

    $firstwhere = ' WHERE ';
    $nextwhere = ' AND ';

    if ($period == 'year'){
      $query .= " $firstwhere YEAR(t.`dt`) = YEAR(CURDATE()) ";
    }
    else if ($period == 'month'){
      $query .= " $firstwhere MONTH(t.`dt`) = MONTH(CURDATE()) ";
    }
    else if ($period == 'week'){     
      //$query .= " WHERE YEARWEEK(t.`dt`, 1) = YEARWEEK(CURDATE(), 0) ";
      $query .= " $firstwhere YEARWEEK(t.`dt`) = YEARWEEK(CURDATE()) ";
    }
    else if ($period == 'today'){     
      $query .= " $firstwhere DAY(t.`dt`) = DAY(CURDATE()) ";
    }
    else {
      // do nothing
    }

    //

    if ($type == 'parts'){
      $query .= " $nextwhere t.`subject` LIKE '%New Parts Order%' ";

    }
    else if ($type == 'service'){
      $query .= " $nextwhere t.`subject` LIKE '%New Service Enquiry%' ";
    } 
    else if ($type == 'general'){
      $query .= " $nextwhere t.`subject` LIKE '%New General Enquiry%' ";
    }
    else if ($type == 'product_enquiry'){
      $query .= " $nextwhere t.`subject` LIKE '%New Vehicle Purchase Enquiry%' ";
    }
    else if ($type == 'book_test'){
      $query .= " $nextwhere t.`subject` LIKE '%New Book A Test Drive Enquiry%' ";
    }
    else {
      // do nothing
    }

    //echo $query;

    $res = hesk_dbQuery($query);
    //$res = hesk_dbQuery("SELECT * FROM `hesk_tickets` WHERE `staffreplies` = '0' AND `closedat` IS NULL");
    
    $tickets = array();

    while ($tct = hesk_dbFetchAssoc($res))
    {
      $tickets[] = $tct; 
    }

    return $tickets;  
}
function get_categories(){
    global $hesk_settings, $hesklang, $ticket;
    $res = hesk_dbQuery("SELECT `id`, `name` FROM `hesk_categories` ");
    
    $rows = array();

    while ($tct = hesk_dbFetchAssoc($res))
    {
      $rows[] = $tct; 
    }

    return $rows;  
}

$tickets = get_tickets('today', 'service');
$categories = get_categories();
$tickets_thisweek = get_tickets('week', 'service');
$tickets_thismonth = get_tickets('month', 'service');
$tickets_thisyear = get_tickets('year', 'service');

// echo '<pre>';
// //print_r($tickets);
// echo '</pre>';

$closed_tickets = array_filter($tickets, function($ticket){
  return $ticket['status'] == '3';
});

$open_tickets = array_filter($tickets, function($ticket){
  return $ticket['status'] != '3';
});

// echo '<pre>';
// //print_r($open_tickets);
// echo '</pre>';

$users_by_tickets = array();
$team_tickets = array();

foreach ($tickets as $ticket) {
  $users_by_tickets[$ticket['staffname']][] = $ticket;
}

foreach ($tickets as $ticket) {
  //$team_tickets[$ticket['branchname'][$ticket['categoryname']]][] = $ticket;
  $team_tickets[$ticket['branchname']][$ticket['categoryname']][] = $ticket;
}

// echo '<pre>';
// //print_r($team_tickets);
// //print_r($team_tickets['Nairobi Toyota']['Parts']);
// echo '</pre>';

?>
<h3 style="text-align:center;">Service Enquiries Report <?php echo Date('Y-m-d') ?></h3>
<hr>
<p></p>
<p></p>
<h3>Number of Tickets Today: <?php echo count($tickets); ?></h3>
<p></p>
<p></p>
<h3>Number of Tickets This Week: <?php echo count($tickets_thisweek); ?></h3>
<p></p>
<p></p>
<h3>Number of Tickets This Month: <?php echo count($tickets_thismonth); ?></h3>
<p></p>
<p></p>
<h3>Number of Tickets This Year: <?php echo count($tickets_thisyear); ?></h3>
<p></p>
<p></p>
<h3>Number of  Tickets by Team</h3>
<p></p>
<table class="white" cellspacing="1" cellpadding="3" border="0">
  <thead>
    <tr>
      <th class="admin_white">Branch</th>
      
      <?php 
        // foreach ($categories as $category) {
        //   echo '<th class="admin_white">'.$category['name'].'</th>';
        // }
        echo '<th class="admin_white">Service</th>';
      ?>
    </tr>
  </thead>
  <tbody>
    <?php 
      foreach ($team_tickets as $branch => $tickets_in_cats) { 
        // $$tickets_in_cats is array of tickets
        echo '<tr>';
        echo '<td class="admin_white">'.$branch.'</td>';
        // foreach ($categories as $category) {
        //   $number = isset($tickets_in_cats['Service']) ? count($tickets_in_cats[$category['name']]) : '0';
        //   echo '<td class="admin_white">'.$number.'</td>';
        // }
        $number = isset($tickets_in_cats['Service']) ? count($tickets_in_cats['Service']) : '0';
        echo '<td class="admin_white">'.$number.'</td>';
        echo '</tr>';
      ?>
    
    <?php 
      }
    ?>
  </tbody>
</table>
<p></p>
<p></p>
<h3>Number of Open Tickets: <?php echo count($open_tickets); ?></h3>
<p></p>
<p></p>
<table class="white" cellspacing="1" cellpadding="3" border="0">
  <thead>
    <tr>
      <th class="admin_white">Ticket ID</th>
      <th class="admin_white">Received By</th>
      <th class="admin_white">Customer</th>
      <th class="admin_white">Email</th>
      <th class="admin_white">Subject</th>
      <th class="admin_white">Message</th>
      <th class="admin_white">Branch</th>
      <th class="admin_white">Category</th>
      <th class="admin_white">Status</th>
      <th class="admin_white">Closed At</th>
      <th class="admin_white">Action Pending</th>
    </tr>
  </thead>
  <tbody>
    <?php 
      foreach ($open_tickets as $user => $ticket) { 
      ?>
    <tr>
      <td class="admin_white"><?php echo $ticket['trackid']; ?></td>
      <td class="admin_white"><?php echo isset($ticket['staffname']) ? $ticket['staffname'] : 'Unassigned'; ?></td>
      <td class="admin_white"><?php echo $ticket['name']; ?></td>
      <td class="admin_white"><?php echo $ticket['email']; ?></td>
      <td class="admin_white"><?php echo $ticket['subject']; ?></td>
      <td class="admin_white"><?php echo $ticket['message']; ?></td>
      <td class="admin_white"><?php echo $ticket['branchname']; ?></td>
      <td class="admin_white"><?php echo $ticket['categoryname']; ?></td>
      <td class="admin_white"><?php echo hesk_get_status_name($ticket['status']); ?></td>
      <td class="admin_white"><?php echo $ticket['closedat']; ?></td>
      <td class="admin_white"><?php echo $ticket['staffreplies']; ?></td>
    </tr>
    <?php 
      }
    ?>
  </tbody>
</table>
<p></p>
<p></p>
<h3>Closed Tickets: <?php echo count($closed_tickets); ?></h3>
<p></p>
<p></p>
<table class="white" cellspacing="1" cellpadding="3" border="0">
  <thead>
    <tr>
      <th class="admin_white">Ticket ID</th>
      <th class="admin_white">PIC</th>
      <th class="admin_white">Customer</th>
      <th class="admin_white">Email</th>
      <th class="admin_white">Subject</th>
      <th class="admin_white">Message</th>
      <th class="admin_white">Branch</th>
      <th class="admin_white">Category</th>
      <th class="admin_white">Status</th>
      <th class="admin_white">Closed At</th>
      <th class="admin_white">PIC Replies</th>
    </tr>
  </thead>
  <tbody>
    <?php 
      foreach ($closed_tickets as $user => $ticket) { 
      ?>
    <tr>
      <td class="admin_white"><?php echo $ticket['trackid']; ?></td>
      <td class="admin_white"><?php echo isset($ticket['staffname']) ? $ticket['staffname'] : 'Unassigned'; ?></td>
      <td class="admin_white"><?php echo $ticket['name']; ?></td>
      <td class="admin_white"><?php echo $ticket['email']; ?></td>
      <td class="admin_white"><?php echo $ticket['subject']; ?></td>
      <td class="admin_white"><?php echo $ticket['message']; ?></td>
      <td class="admin_white"><?php echo $ticket['branchname']; ?></td>
      <td class="admin_white"><?php echo $ticket['categoryname']; ?></td>
      <td class="admin_white"><?php echo hesk_get_status_name($ticket['status']); ?></td>
      <td class="admin_white"><?php echo $ticket['closedat']; ?></td>
      <td class="admin_white"><?php echo $ticket['staffreplies']; ?></td>
    </tr>
    <?php 
      }
    ?>
  </tbody>
</table>
